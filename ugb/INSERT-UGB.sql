USE ugb;
INSERT INTO  utilisateur (mot_de_passe, mail) 
VALUES ('ilovedandd','pierredu59@mail.com'),
		('sakurajtm','sasuke@mail.com'),
        ('truc','machin@mail.com'),
        ('azerty','bg@mail.com'),
        ('azerty','pablo@mail.com'),
        ('azerty','thierry@mail.com');
INSERT INTO joueur (pseudo, utilisateur_id)
VALUES ("pierrot", 1),
		("saku",2),
		("tutu",3),
		("BeauGosse", 4),
		("pablo", 5),
		("thierry", 6);
INSERT INTO log (texte, date)
VALUES ('29', "2019-09-12 12:48:59"),
		('00', "2018-11-29 22:34:12"),
		('47', "2020-7-08 3:01:47");

INSERT INTO  partie (nom, date_creation) 
VALUES ('DandD', '2020-04-25 12:48:59'),
		('super_game', '2020-04-26 12:48:59'),
        ('ultimate_partie', '2020-04-27 12:48:59');
INSERT INTO appartenir (maitre_jeu, partie_id, joueur_id)
VALUES (1,1,1),
		(0,1,2),
		(0,1,3),
		(1,2,3),
		(0,2,4),
		(0,2,5),
		(1,3,2),
		(0,3,3),
		(0,3,6);
INSERT INTO plateau (partie_id)
VALUES (1),
		(1),
		(1),
        (1),
		(2),
		(2),
		(3),
		(3),
		(3);
INSERT INTO personnage (plateau_id, appartenir_id, nom, x, y, longueur, largeur)
VALUES (1, 1, "Ogre", 10, 10, 2, 2),
		(3, 2, "Sakura", 2, 5, 1, 1),
		(3, 3, "SuperTruc", 2, 7, 1, 1),
		(2, 1, "Goblin1", 18, 5, 1, 1),
		(2, 1, "Goblin2", 18, 5, 1, 1),
        (3, 1, "Tarasque", 10, 10, 2, 2),
		(4, 1, "Dragon", null, null, 2, 3),
		(5, 4, "Gros dragon", 10, 10, 3, 4),
		(5, 5, "BeauGosse", 9, 9, 1, 1),
		(5, 6, "Pablito", 14, 13, 1, 1),
		(6, 7, "Ogre", null, null, 2, 2),
		(7, 7, "Troll", null, null, 2, 2),
		(8, 7, "Troll1", 5, 1, 2, 2),
		(8, 7, "Troll2", 5, 5, 2, 2),
		(8, 7, "Chef troll", 7, 3, 2, 2),
		(8, 8, "Trucisime", 3, 10, 1, 1),
		(8, 9, "Titi", 5, 10, 1, 1);
INSERT INTO jet_des (resultat, calcul, date, personnage_id)
VALUES ("26", "4d6+16", "2020-5-15 3:02:00", 1),
        ("36", "4d6+16",  "2020-5-15 3:03:00", 1),
        ("26", "6d6",  "2020-5-15 3:04:00", 2),
        ("15", "2d6+6",  "2020-5-15 3:05:00", 3),
        ("12", "2d6+6",  "2020-5-15 3:05:30", 3),
        ("6", "1d6+4",  "2020-5-15 3:03:00", 4),
        ("5", "1d6+4",  "2020-5-15 3:03:00", 5),
        ("24", "6d6",  "2020-5-15 3:03:30", 2),
        ("9", "2d6+6",  "2020-5-15 3:04:00", 3),
        ("11", "2d6+6",  "2020-5-15 3:04:30", 3),
        ("31", "6d6+16",  "2020-5-15 3:03:00", 6);

INSERT INTO message (personnage_id, texte, date)
VALUES (1, 'Grr', '2020-5-15 3:00:00'),
		(1, 'Moi manger vous', '2020-5-15 3:01:00'),
		(4, 'Vous partir !', '2020-5-15 3:02:00'),
		(5, 'Ou vous morts !', '2020-5-15 3:02:30'),
		(8, "Vous n\'aurriez pas dut venir", '2020-5-15 4:10:00');







