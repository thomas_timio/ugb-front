import { Message }  from '../models/Message.js';
import { BehaviorSubject } from 'rxjs';

class ChatService {

    _messageSub = new BehaviorSubject([])
    messageObs = this._messageSub.asObservable()

    getAllMessages = () => {
        console.log("OK")
        fetch('http://localhost:8090/voitures')
        .then(res => res.json())
        .then(data => data.map(v => new Message(v.id, v.marque, v.couleur)))
        .then(messages => this._messageSub.next(messages))
        .catch(console.log)
        
    }
}

export default Object.freeze(new ChatService());