import React from 'react';
import './App.css';
import { Routing } from './components/common/Routing';
import TopMenu from './components/TopMenu';
import 'primereact/resources/themes/nova-dark/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

function App() {
  return (
    <Routing>
      <TopMenu />
    </Routing>
  );
}

export default App;
