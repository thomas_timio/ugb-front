import React, { Fragment } from 'react';
import ImageRonde from '../../components/ImageRonde'
import {Panel} from 'primereact/panel';
import {InputText} from 'primereact/inputtext';
import {Button} from 'primereact/button';
import {Password} from 'primereact/password';
import './profil.css'

const Profil = props => {
    return (
        <Fragment>
            <div className="top">
                <h1>PROFIL</h1>
                <ImageRonde style={{height : '200px'}}/>
            </div>
            <div className="profilContent">
            <Panel header='INFORMATIONS' className="panelInfo">
                <form className="formProfil">
                    <div className="elemText">
                        <label>Pseudonyme</label>
                        <InputText/>
                    </div>
                    <div className="elemText">
                        <label>Email</label>
                        <InputText/>
                    </div>
                <Button label="Modifier" className="p-button-raised p-button-danger" style={{width:"100px", backgroundColor:"#BA3131"}}/>
                </form>
            </Panel>
            <Panel header='SECURITE'>
                <form className="formProfil">
                    <div className="elemText">
                        <label>Mot de passe actuel</label>
                        <Password feedback={false} />
                    </div>
                    <div className="elemText">
                        <label>Nouveau mot de passe</label>
                        <Password />
                    </div>
                    <div className="elemText">
                        <label>Confirmer mot de passe</label>
                        <Password feedback={false} />
                    </div>
                <Button label="Modifier" className="p-button-raised p-button-danger" style={{width:"100px", backgroundColor:"#BA3131"}}/>
                </form>
            </Panel>
            </div>
        </Fragment>
    );
};

export default Profil;