import React, { Fragment } from "react";
import StageComp from "../../components/StageComp";
import MenuOutilsPartie from "../../components/MenuOutilsPartie";
import './partie.css'

const Partie = props => {

  const disposition = {
    display : 'flex'
  }

  return (
    <Fragment>
      <div style={disposition}>
        <MenuOutilsPartie />
        <StageComp></StageComp>
      </div>
    </Fragment>
  );
};

export default Partie;
