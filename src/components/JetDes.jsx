import React, { Component } from 'react';

export class JetDes extends Component {
    constructor(props) {
      super(props);
      this.state = {value: ''};
  
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleChange(event) {
      this.setState({value: event.target.value});
    }
  
    handleSubmit(event) {
      alert('Les des ont été lancés : ' + this.state.value);
      event.preventDefault();
    }
  
    render() {
      return (
        <form onSubmit={this.handleSubmit}>
          <label>
            Nombre :
            <input type="text" value={this.state.value} onChange={this.handleChange} />
          </label>

          <label>
          Type :
          <select value={this.state.value} onChange={this.handleChange}>
            <option value="3">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="6">6</option>
            <option value="8">8</option>
            <option value="8">10</option>
            <option value="8">12</option>
            <option value="8">16</option>
            <option value="8">20</option>
            <option value="8">24</option>
            <option value="8">30</option>
            <option value="3">100</option>
          </select>
        </label>

          <label>
            Plus :
            <input type="text" value={this.state.value} onChange={this.handleChange} />
          </label>

          <input type="submit" value="Lancer" />
        </form>
      );
    }
  }