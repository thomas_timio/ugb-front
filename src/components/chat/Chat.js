import React, { Fragment} from 'react';
import MessageList from './MessageList';
import FormMessage from './FormMessage'

//import VoitureService from '../services/VoitureService';

const Chat = (props) => {


    return (
        <Fragment>
            <h2>Messages</h2>
            <MessageList />
            <h2>Envoyer message</h2>
            <FormMessage />
        </Fragment>
    );
};

export default Chat;