import React, { Fragment, useState } from 'react';

const FormMessage = (props) => {
    
    const [marque, setMarque] = useState("");
    const [couleur, setCouleur] = useState("");
    

    const handleChangeMarque = (event) => {
        setMarque(event.target.value)
    }

    const handleChangeCouleur = (event) => {
        setCouleur(event.target.value)
    }

    const handleClick = (event) => {
        event.preventDefault()
            let msg = { "marque" : marque, "couleur" : couleur }
        console.log(JSON.stringify(msg))
        const requestOptions = {
            method : 'POST',
            headers : {'Content-Type': 'application/json'},
            body: JSON.stringify(msg)
        }
        
        fetch('http://localhost:8090/voitures', requestOptions)
            .then(console.log('OK'))

        setCouleur("")
        setMarque("")
    }

    return (
        <form onSubmit = {handleClick}>
            <label>Marque:</label>
            <input type='text' onChange={handleChangeMarque} value={marque} ></input>
            <label>Couleur</label>
            <input type='text' onChange={handleChangeCouleur} value={couleur} ></input>
            <button >Clique</button>
        </form>
        );
};

export default FormMessage;