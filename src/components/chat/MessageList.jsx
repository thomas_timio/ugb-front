import React, { Fragment, useState, useEffect  } from 'react';
import MessageRender from './MessageRender';
import ChatService from '../../services/ChatService';

const MessageList = () => {
    const [messages, setMessages] = useState([]);

    useEffect(() => {
        let messageSub = ChatService.messageObs.subscribe(messages => setMessages(messages))
        ChatService.getAllMessages()
        return () => {
            messageSub.unsubscribe()
        }
    }, [setMessages])

    setTimeout(() => {
        ChatService.getAllMessages()
    }, 2000)
    
    return (
        <Fragment>
            {messages?.map((msg, index) => <MessageRender key={index} marque={msg.marque} couleur={msg.couleur}/>)}
        </Fragment>
    );
};

export default MessageList;