import React, { Fragment } from 'react';

const ImageRonde = (props) => {

  const borderRadiusImg = 50 + '% ';
      
  const imgUrl = 'https://cdna.artstation.com/p/assets/images/images/024/495/472/large/patri-balanovsky-one-shape-50-det-3.jpg?1582617504';

  const styleImg = {
    borderRadius: borderRadiusImg,
    width: props.style.height,
    height: props.style.height,
    margin: '7px',
    backgroundImage: `url(${imgUrl})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    alignItems:'center',
    alignContent:'center',
    display:'flex',
    flexDirection:'column'
  }

  return (
    <Fragment>
      <img src={imgUrl} style={styleImg} alt='Profil'></img>  
    </Fragment>
  );
};

export default ImageRonde;