import Rebase from 're-base'
import firebase from 'firebase/app'
import 'firebase/database'

const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyDPXRMjwwetzkbmy8wwwSsPfFPYzCcaprI",
    authDomain: "ultimategameboard.firebaseapp.com",
    databaseURL: "https://ultimategameboard.firebaseio.com"
})

const base = Rebase.createClass(firebase.database())

export { firebaseApp }

export default base