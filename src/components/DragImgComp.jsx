import React, { Fragment } from "react";
import { Image } from "react-konva";
import useImage from "use-image";
import ripndip from "../assets/ripndip.jpg";

const ImageUseUrl = () => {
  const [image] = useImage("https://konvajs.org/assets/lion.png");
  const x = 200;
  const y = 200;
  const w = 100;
  const h = 100;
  return <Image image={image} x={x} y={y} width={w} height={h} draggable />;
};

const ImageUseLocal = () => {
  const [image] = useImage(ripndip);
  const x = 450;
  const y = 300;
  const w = 100;
  const h = 100;
  return <Image image={image} x={x} y={y} width={w} height={h} draggable />;
};

export const DragImg = () => {
  return (
    <Fragment>
      <ImageUseUrl />
      <ImageUseLocal />
    </Fragment>
  );
};
export default DragImg;
