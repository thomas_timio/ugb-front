import React, { Component } from "react";
import { Stage, Layer } from "react-konva";
import DragImg from "./DragImgComp";
import BackgroundMap from "./BackgroundMap";
import map1 from "./../assets/map1.jpg";
import map2 from "./../assets/map2.jpg";
import map3 from "./../assets/map3.jpg";

export default class StageComp extends Component {
  render() {
    return (
      <div
        style={{
          display: "flex",
          width: "100%",
          height: "100vh",
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Stage
          width={900}
          height={600}
          style={{
            border: "2px solid red",
            backgroundColor: "black"
            /*width: "50%",
            height: "50vh"*/
          }}
        >
          <Layer>
            <BackgroundMap src={map2} />
            <DragImg />
          </Layer>
        </Stage>
      </div>
    );
  }
}
