import React, {Component} from 'react';
import {Card} from 'primereact/card';

export class ItemsPlateau extends Component {
    render() {
        const card = {
            width: '80%',
        };

        const header = ( <img alt="Card" src='https://github.com/primefaces/primereact/blob/master/src/resources/images/primereact-slider-bg.jpg?raw=true'/>
        );

        return(
            <Card style={card} header={header} title="plateau">
            </Card>
        );
    }
}