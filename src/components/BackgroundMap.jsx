import React from "react";
import { Image } from "react-konva";
import useImage from "use-image";

export const BackgroundMap = props => {
  const [img] = useImage(props.src);
  let newWidth;
  let newHeight;

  if (img) {
    let proportion = img.width / img.height;
    if (proportion === 1.5) {
      newWidth = 900;
      newHeight = 600;
    } else if (proportion > 1.5) {
      newWidth = 900;
      newHeight = 900 / proportion;
    } else if (proportion < 1.5) {
      newHeight = 600;
      newWidth = 600 * proportion;
    }
  }

  return (
    <Image
      image={img}
      x={450}
      y={300}
      width={newWidth}
      height={newHeight}
      offsetX={newWidth / 2}
      offsetY={newHeight / 2}
    />
  );
};

export default BackgroundMap;
