import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import Profil from '../../pages//Profil/Profil';
import Partie from '../../pages/Partie/Partie';
import Chat from '../chat/Chat'
import Accueil from '../../pages/Accueil/Accueil';


export const Routing = props => {
    return (
        <Router>
            {props.children}
            <Switch>
                <Route path="/chat" component={Chat}/>
                <Route path="/accueil" component={Accueil}/>
                <Route path="/profil" component={Profil}/>
                <Route path="/partie" component={Partie}/>
                <Route path="/" component={ () => <Redirect to="/accueil" />} />
            </Switch>
        </Router>
    )
}