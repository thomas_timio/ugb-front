import React, { Fragment } from 'react';
import logo from '../assets/logo_blanc.svg';
import ImageRonde from './ImageRonde';
import { Link } from 'react-router-dom';

const TopMenu = () => {

    const menu = {
        display : 'flex',
        backgroundColor : '#BA3131',
        justifyContent : 'space-between',
    }

    const logoStyle = {
        fill : 'white',
        height : '40px',
        margin : '7px'
    }

    const imgProfilStyle = {
        height : '40px',
        margin : '7px'
    }

    return (
        <Fragment>
            <div style={menu}>
                <Link to='/accueil'>
                    <img src={logo} alt='logo' style={logoStyle} />
                </Link>
                <Link to='/parties'>
                    <p>Parties</p>
                </Link>
                <Link to='/profil'>
                    <ImageRonde style={imgProfilStyle} />
                </Link>
            </div>
        </Fragment>
    );
};

export default TopMenu;