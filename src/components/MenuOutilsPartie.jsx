import React, { Fragment, useState } from 'react';
import { Button } from 'primereact/button';
import {Sidebar} from 'primereact/sidebar';
import {Fieldset} from 'primereact/fieldset';
import {JetDes} from './JetDes';

const MenuOutilsPartie = () => {

    const menu = {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100%',
        backgroundColor: '#e91224',
        zIndex: '9999'
    }

    const btnStyle = {
        fontSize : '1.75em', 
        backgroundColor : '#BA3131',
        marginBottom : '3px',
        marginTop : '3px'
    }

    const [header, setHeader] = useState('');
    const [visible, setVisibility] = useState(false);
    const [content, setContent] = useState();

    const onSelect= (event) => {
        if(event.currentTarget.id === "2") {
           setContent(<JetDes/>)
        }
        setVisibility(true);
        setHeader(event.currentTarget.name);
    }

    const onHide = () => {
        setVisibility(false);
    }

    return (
        <Fragment>
            <div style={menu}>
                <Button id="1" name='joueurs' icon="pi pi-check" className="p-button-danger" tooltip="Joueurs" style={btnStyle} onClick={onSelect}> </Button>
                <Button id="2" name='jet de dés' icon="pi pi-check" className="p-button-danger" tooltip="Jet de dés" style={btnStyle} onClick={onSelect}> </Button>
                <Button id="3" name='plateaux' icon="pi pi-check" className="p-button-danger" tooltip="Plateaux" style={btnStyle} onClick={onSelect}> </Button>
                <Button id="4" name='personnages' icon="pi pi-check" className="p-button-danger" tooltip="Personnages" style={btnStyle} onClick={onSelect}> </Button>
            </div>

            <Sidebar visible={visible}
                style={{width:'30em', marginLeft:"65.9px", marginTop:"58px", background:'#fffbfa', zIndex:0}}
                modal={false}
                dismissable={true}
                onHide={onHide}>
                <Fieldset legend={header.toUpperCase()}>
                    {content}
                </Fieldset>
            </Sidebar>
            
        </Fragment>
    );
};

export default MenuOutilsPartie;